package com.curd.operation.springbootcurdhibernateexample.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.curd.operation.springbootcurdhibernateexample.domain.Product;

@SpringBootTest
@DisplayName("When Running Product Service Class")
class ProductServiceTest {

	@MockBean
	private ProductService productService;

	Product product;

	@BeforeEach
	void init() {
		product = new Product();
	}

	@Test
	@DisplayName("Testing GetProductById Method")
	void testGetProductById() {
		Long id = (long) 1;
		product.setId(id);
		when(productService.getProductById(id)).thenReturn(product);
		assertEquals(id, productService.getProductById(id).getId());
	}

	@Test
	@DisplayName("Testing GetAllProductDetials Method")
	void testGetAllProductDetials() {
		Long id = (long) 1;
		product.setId(id);
		when(productService.getAllProductDetials()).thenReturn(Stream.of(product).collect(Collectors.toList()));
		assertEquals(1, productService.getAllProductDetials().size());
	}

	@Test
	@DisplayName("Testing SaveProduct Method")
	void testSaveProduct() {
		Long id = (long) 1;
		product.setId(id);
		when(productService.saveProduct(product)).thenReturn(product);
		assertEquals(id, productService.saveProduct(product).getId());
	}

	@Test
	@DisplayName("Testing UpdateProduct Method")
	void testUpdateProduct() {
		Long id = (long) 1;
		product.setId(id);
		when(productService.updateProduct(product)).thenReturn(product);
		assertEquals(id, productService.updateProduct(product).getId());
	}

	@Test
	@DisplayName("Testing DeleteProductById Method")
	void testDeleteProductById() {
		Long id = (long) 1;
		productService.deleteProductById(id);
		verify(productService, times(1)).deleteProductById(id);
	}

}
