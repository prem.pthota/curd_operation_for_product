package com.curd.operation.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@Configuration
@SecurityScheme(name = "Authorization", scheme = "basic", type = SecuritySchemeType.APIKEY, in = SecuritySchemeIn.HEADER)
public class SwaggerConfig {
	
	 @Bean
	  public OpenAPI apiInfo() {
	      return new OpenAPI()
	              .info(new Info().title("SpringCurd API")
	              .description("This is a `Curd`authentication service. Once you have successfully logged in and obtained the token, you should click on the right top button `Authorize` and introduce it with the prefix `Bearer`.")
	              .version("1.0.0"));
	  }
}
