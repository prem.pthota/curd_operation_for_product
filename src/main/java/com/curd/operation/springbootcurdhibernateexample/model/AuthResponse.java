package com.curd.operation.springbootcurdhibernateexample.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthResponse {
	private String token;
	private String type = "Bearer";
	private Long id;

	public AuthResponse(String accessToken, Long id) {
		this.token = accessToken;
		this.id = id;
	}
}
