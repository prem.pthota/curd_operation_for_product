package com.curd.operation.springbootcurdhibernateexample.model;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {
	@NotBlank(message = "User Name is mandatory")
    private String username;
	@NotBlank(message = "Password is mandatory")
    private String password;
}
