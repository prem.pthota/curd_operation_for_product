package com.curd.operation.springbootcurdhibernateexample.model;

import com.curd.operation.springbootcurdhibernateexample.domain.User;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserResponse {
	private User user;
}
