package com.curd.operation.springbootcurdhibernateexample.service;

import javax.servlet.http.HttpServletRequest;

import com.curd.operation.springbootcurdhibernateexample.model.AuthResponse;
import com.curd.operation.springbootcurdhibernateexample.model.LoginRequest;
import com.curd.operation.springbootcurdhibernateexample.model.UserResponse;

public interface AuthService {
	AuthResponse authorize(LoginRequest loginRequest);

	AuthResponse refresh(HttpServletRequest req);

	AuthResponse authorizeWithJWT(HttpServletRequest req);

	AuthResponse authorizeWithToken(String token);
	
	UserResponse currentUser(HttpServletRequest req);
}
