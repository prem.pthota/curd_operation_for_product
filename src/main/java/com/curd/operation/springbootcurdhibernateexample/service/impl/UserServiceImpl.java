package com.curd.operation.springbootcurdhibernateexample.service.impl;

import java.lang.module.ResolutionException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curd.operation.springbootcurdhibernateexample.domain.User;
import com.curd.operation.springbootcurdhibernateexample.repository.UserRepository;
import com.curd.operation.springbootcurdhibernateexample.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;

	@Override
	public User saveUser(User user) {
		return this.userRepository.save(user);
	}

	@Override
	public User getUserById(Long id) {
		Optional<User> userDb = this.userRepository.findById(id);
		if (userDb.isPresent()) {
			return userDb.get();
		} else {
			throw new ResolutionException("Record not found with user id : " + id);
		}
	}

	@Override
	public List<User> getAllUserDetials() {
		return userRepository.findAll();
	}

	@Override
	public User updateUser(User user) {
		Optional<User> userDb = this.userRepository.findById(user.getId());
		if (userDb.isPresent()) {
			userDb.get().setId(user.getId());
			userDb.get().setName(user.getName());
			userDb.get().setLoginId(user.getLoginId());
			userDb.get().setPassword(user.getPassword());
			userDb.get().setToken(user.getPassword());
			return this.userRepository.saveAndFlush(userDb.get());
		} else {
			throw new ResolutionException("Record not found with user id : " + user.getId());
		}
	}

	@Override
	public void deleteUserById(Long id) {
		Optional<User> userDb = this.userRepository.findById(id);
		if (userDb.isPresent()) {
			this.userRepository.delete(userDb.get());
		} else {
			throw new ResolutionException("Record not found with user id : " + id);
		}

	}
	
	@Transactional
	@Override
	public void deleteUsersWithIds(List<Long> ids) {
		try {
			this.userRepository.deleteUsersWithIds(ids);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
