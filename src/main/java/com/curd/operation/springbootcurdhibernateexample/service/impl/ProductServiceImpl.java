package com.curd.operation.springbootcurdhibernateexample.service.impl;

import java.lang.module.ResolutionException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curd.operation.springbootcurdhibernateexample.domain.Product;
import com.curd.operation.springbootcurdhibernateexample.repository.ProductRepository;
import com.curd.operation.springbootcurdhibernateexample.service.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public Product getProductById(Long id) {
		Optional<Product> productDb = this.productRepository.findById(id);
		if (productDb.isPresent()) {
			return productDb.get();
		} else {
			throw new ResolutionException("Record not found with product id : " + id);
		}
	}

	@Override
	public List<Product> getAllProductDetials() {

		return this.productRepository.findAll();
	}

	@Override
	public Product saveProduct(Product product) {

		return this.productRepository.save(product);
	}

	@Override
	public Product updateProduct(Product product) {
		Optional<Product> productDb = this.productRepository.findById(product.getId());
		if (productDb.isPresent()) {
			productDb.get().setId(product.getId());
			productDb.get().setName(product.getName());
			productDb.get().setDescription(product.getDescription());
			return this.productRepository.saveAndFlush(productDb.get());
		} else {
			throw new ResolutionException("Record not found with product id : " + product.getId());
		}
	}

	@Override
	public void deleteProductById(Long id) {
		Optional<Product> productDb = this.productRepository.findById(id);
		if (productDb.isPresent()) {
			this.productRepository.delete(productDb.get());
		} else {
			throw new ResolutionException("Record not found with product id : " + id);
		}

	}

	@Transactional
	@Override
	public void deleteProductsWithIds(List<Long> ids) {
		try {
			this.productRepository.deleteProductsWithIds(ids);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
