package com.curd.operation.springbootcurdhibernateexample.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import com.curd.operation.springbootcurdhibernateexample.domain.User;
import com.curd.operation.springbootcurdhibernateexample.exception.AuthException;
import com.curd.operation.springbootcurdhibernateexample.model.AuthResponse;
import com.curd.operation.springbootcurdhibernateexample.model.LoginRequest;
import com.curd.operation.springbootcurdhibernateexample.model.UserResponse;
import com.curd.operation.springbootcurdhibernateexample.repository.UserRepository;
import com.curd.operation.springbootcurdhibernateexample.security.JwtTokenProvider;
import com.curd.operation.springbootcurdhibernateexample.service.AuthService;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Override
	public AuthResponse authorize(LoginRequest loginRequest) {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
			User user = userRepository.findByLoginId(loginRequest.getUsername());
			return new AuthResponse(jwtTokenProvider.createToken(user), user.getId());
		} catch (AuthenticationException e) {
			throw new AuthException("InvalidLogin", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public AuthResponse refresh(HttpServletRequest req) {
		try {
			String username = jwtTokenProvider.getUsername(jwtTokenProvider.resolveRefreshToken(req));
			User user = userRepository.findByLoginId(username);
			return new AuthResponse(jwtTokenProvider.createToken(user), user.getId());

		} catch (AuthenticationException e) {
			throw new AuthException("Invalid token supplied", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public AuthResponse authorizeWithJWT(HttpServletRequest req) {

		try {
			String username = jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req));
			User user = userRepository.findByLoginId(username);
			return new AuthResponse(jwtTokenProvider.createToken(user), user.getId());

		} catch (AuthenticationException e) {

			throw new AuthException("Invalid token supplied", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public AuthResponse authorizeWithToken(String token) {
		try {
			User user = userRepository.findByToken(token);
			return new AuthResponse(jwtTokenProvider.createToken(user), user.getId());

		} catch (AuthenticationException e) {

			throw new AuthException("Invalid token supplied", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public UserResponse currentUser(HttpServletRequest req) {
		try {
			return new UserResponse(
					userRepository.findByLoginId(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req))));
		} catch (AuthenticationException e) {
			throw new AuthException("Invalid token supplied", HttpStatus.UNPROCESSABLE_ENTITY);
		}

	}

}
