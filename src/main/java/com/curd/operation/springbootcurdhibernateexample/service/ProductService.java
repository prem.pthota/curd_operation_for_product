package com.curd.operation.springbootcurdhibernateexample.service;

import java.util.List;

import com.curd.operation.springbootcurdhibernateexample.domain.Product;

public interface ProductService {

	Product getProductById(Long id);

	List<Product> getAllProductDetials();

	Product saveProduct(Product product);

	Product updateProduct(Product product);

	void deleteProductById(Long id);
	
	void deleteProductsWithIds(List<Long> ids);
}
