package com.curd.operation.springbootcurdhibernateexample.service;

import java.util.List;

import com.curd.operation.springbootcurdhibernateexample.domain.User;

public interface UserService {
	User saveUser(User user);

	User getUserById(Long id);

	List<User> getAllUserDetials();

	User updateUser(User user);

	void deleteUserById(Long id);
	
	void deleteUsersWithIds(List<Long> ids);
}
