package com.curd.operation.springbootcurdhibernateexample.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.curd.operation.springbootcurdhibernateexample.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByLoginId(String loginId);

	User findByToken(String token);
	
	@Modifying
	@Query("delete from User u where u.id in ?1")
	void deleteUsersWithIds(List<Long> ids);
}
