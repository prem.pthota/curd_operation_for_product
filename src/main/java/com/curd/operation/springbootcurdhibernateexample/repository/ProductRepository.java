package com.curd.operation.springbootcurdhibernateexample.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.curd.operation.springbootcurdhibernateexample.domain.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	@Modifying
	@Query("delete from Product p where p.id in ?1")
	void deleteProductsWithIds(List<Long> ids);
}
	