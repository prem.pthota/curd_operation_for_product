package com.curd.operation.springbootcurdhibernateexample.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.curd.operation.springbootcurdhibernateexample.exception.AuthException;

public class JwtTokenFilter extends OncePerRequestFilter {

	private JwtTokenProvider jwtTokenProvider;

	public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
		this.jwtTokenProvider = jwtTokenProvider;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			FilterChain filterChain) throws ServletException, IOException {
		String token = jwtTokenProvider.resolveToken(httpServletRequest);
		try {
			if (httpServletRequest.getRequestURI().endsWith("refresh")) {
				String refreshToken = jwtTokenProvider.resolveRefreshToken(httpServletRequest);
				if (refreshToken != null || jwtTokenProvider.validateToken(refreshToken)) {
					Authentication auth = jwtTokenProvider.getAuthentication(refreshToken);
					SecurityContextHolder.getContext().setAuthentication(auth);
				}

			} else if (httpServletRequest.getRequestURI().endsWith("logout")) {
				String refreshToken = jwtTokenProvider.resolveRefreshToken(httpServletRequest);
				if (refreshToken != null || jwtTokenProvider.validateToken(refreshToken)) {
					Authentication auth = jwtTokenProvider.getAuthentication(refreshToken);
					SecurityContextHolder.getContext().setAuthentication(auth);
				}
				Cookie sessionCookie = new Cookie("jwt-access-token", null);
				sessionCookie.setHttpOnly(true);
				sessionCookie.setPath("/");
				// sessionCookie.setSecure(true);
				sessionCookie.setMaxAge(0);
				httpServletResponse.addCookie(sessionCookie);

			} else if (token != null && jwtTokenProvider.validateToken(token)) {
				Authentication auth = jwtTokenProvider.getAuthentication(token);
				SecurityContextHolder.getContext().setAuthentication(auth);
			}
		} catch (AuthException ex) {

			SecurityContextHolder.clearContext();
			httpServletResponse.sendError(ex.getHttpStatus().value(), ex.getMessage());
			return;
		}

		filterChain.doFilter(httpServletRequest, httpServletResponse);
	}

}
