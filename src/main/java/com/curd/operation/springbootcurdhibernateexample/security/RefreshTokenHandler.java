package com.curd.operation.springbootcurdhibernateexample.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RefreshTokenHandler implements Filter {

	private JwtTokenProvider jwtTokenProvider;

	public RefreshTokenHandler(JwtTokenProvider jwtTokenProvider) {
		this.jwtTokenProvider = jwtTokenProvider;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (((HttpServletRequest) request).getRequestURI().endsWith("authorize")
				|| ((HttpServletRequest) request).getRequestURI().endsWith("authorizeWithJWT")) {
			Cookie sessionCookie = new Cookie("jwt-access-token",
					jwtTokenProvider.getRefreshToken(((HttpServletRequest) request).getHeader("Authorization")));
			sessionCookie.setHttpOnly(true);
			// sessionCookie.setDomain(request.getServerName());
			sessionCookie.setPath("/");
			// sessionCookie.setSecure(true);
			sessionCookie.setMaxAge(1000000);
			((HttpServletResponse) response).addCookie(sessionCookie);
		}
		chain.doFilter(request, response);
	}
}