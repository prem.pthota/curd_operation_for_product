package com.curd.operation.springbootcurdhibernateexample.security;

import java.util.Base64;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.curd.operation.springbootcurdhibernateexample.domain.User;
import com.curd.operation.springbootcurdhibernateexample.exception.AuthException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenProvider {

	@Value("${curd.security.jwt.secret}")
	private String secretKey;

	@Value("${curd.security.jwt.token.expiry}")
	private long validityInMilliseconds;

	@Autowired
	private MyUserDetails myUserDetails;

	@PostConstruct
	protected void init() {
		secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
	}

	public String createToken(User user) {
		Claims claims = Jwts.claims().setSubject(user.getLoginId());
		claims.setId(user.getId().toString());
		Date now = new Date();
		Date validity = new Date(now.getTime() + validityInMilliseconds);

		return Jwts.builder()//
				.setClaims(claims)//
				.setIssuedAt(now)//
				.setExpiration(validity)//
				.signWith(SignatureAlgorithm.HS256, secretKey)//
				.compact();
	}

	public Authentication getAuthentication(String token) {
		UserDetails userDetails = myUserDetails.loadUserByUsername(getUsername(token));
		return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
	}

	public String getUsername(String token) {
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
	}

	public String getUserId(String token) {
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getId();
	}

	public String resolveToken(HttpServletRequest req) {
		String bearerToken = req.getHeader("Authorization");
		if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7);
		}
		return null;
	}

	public String getUserId(HttpServletRequest req) {
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(resolveToken(req)).getBody().getId();
	}

	public String getUsername(HttpServletRequest req) {
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(resolveToken(req)).getBody().getSubject();
	}

	public boolean validateToken(String token) {
		try {
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
			return true;
		} catch (JwtException | IllegalArgumentException e) {
			throw new AuthException("Expired or invalid JWT token", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public String getRefreshToken(String userName) {
		if (userName != null && userName.startsWith("Bearer ")) {
			userName = getUserId(userName.substring(7));
		}
		Date now = new Date();
		return Jwts.builder()//
				.setClaims(Jwts.claims().setSubject(String.valueOf(userName)))//
				.setIssuedAt(now)//
				.signWith(SignatureAlgorithm.HS256, secretKey)//
				.compact();
	}

	public String resolveRefreshToken(HttpServletRequest httpServletRequest) {
		Cookie[] cookies = httpServletRequest.getCookies();
		if (cookies == null || cookies.length < 1) {
			throw new AuthException("Expired or invalid JWT Refresh token", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		String refreshToken = null;
		for (Cookie cookie : cookies) {
			if (("jwt-access-token").equals(cookie.getName())) {
				refreshToken = cookie.getValue();
				break;
			}
		}
		return refreshToken;
	}
}
