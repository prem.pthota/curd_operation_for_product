package com.curd.operation.springbootcurdhibernateexample.controller;

import java.text.MessageFormat;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.curd.operation.springbootcurdhibernateexample.util.PropertyLogUtil;

import io.swagger.v3.oas.annotations.Hidden;
@Hidden
@Controller
public class CurdDefaultView {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private static Properties properties = PropertyLogUtil.getInstance().getProperties();
	
	@Value("${springdoc.swagger-ui.path}")
	private String swagger;
 
	@RequestMapping(value = "/")
	public String page() {
		log.info(MessageFormat.format(properties.getProperty("log.desc.api"), "Swagger"));
		return "redirect:"+swagger;
	}
}
