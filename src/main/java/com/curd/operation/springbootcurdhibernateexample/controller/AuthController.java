package com.curd.operation.springbootcurdhibernateexample.controller;

import java.text.MessageFormat;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curd.operation.springbootcurdhibernateexample.model.AuthResponse;
import com.curd.operation.springbootcurdhibernateexample.model.LoginRequest;
import com.curd.operation.springbootcurdhibernateexample.model.UserResponse;
import com.curd.operation.springbootcurdhibernateexample.service.AuthService;
import com.curd.operation.springbootcurdhibernateexample.util.PropertyLogUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@SecurityRequirement(name = "Authorization")
@RequestMapping({ "/auth" })
@Tag(name = "Auth Controller API", description = "Basic operations for Auth")
public class AuthController {

	@Autowired
	private AuthService authService;
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private static Properties properties = PropertyLogUtil.getInstance().getProperties();

	@PostMapping("/authorize")
	@Operation(summary = "Authorize", responses = {
			@ApiResponse(responseCode = "200", description = "OK", content = @Content(mediaType = "application/json", schema = @Schema(implementation = AuthResponse.class))),
			@ApiResponse(responseCode = "400", description = "Something went wrong", content = @Content),
			@ApiResponse(responseCode = "422", description = "Invalid username/password supplied", content = @Content(schema = @Schema(hidden = true))) }, description = "authorize Details")
	public ResponseEntity<AuthResponse> authorize(@Valid @RequestBody LoginRequest loginRequest) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.login"), loginRequest.getUsername()));
		return ResponseEntity.ok(authService.authorize(loginRequest));
	}

	@GetMapping("/authorizeWithJWT")
	@Operation(summary = "Authorize with JWT", responses = {
			@ApiResponse(responseCode = "200", description = "OK", content = @Content(mediaType = "application/json", schema = @Schema(implementation = AuthResponse.class))),
			@ApiResponse(responseCode = "400", description = "Something went wrong", content = @Content),
			@ApiResponse(responseCode = "422", description = "Invalid username/password supplied", content = @Content(schema = @Schema(hidden = true))) }, description = "Authorize With JWT Details")
	public ResponseEntity<AuthResponse> authorizeWithToken(HttpServletRequest req) {
		log.info("Authorization with token process is triggered");
		return ResponseEntity.ok(authService.authorizeWithJWT(req));
	}

	@GetMapping("/me")
	@Operation(summary = "Me", responses = {
			@ApiResponse(responseCode = "200", description = "OK", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserResponse.class))),
			@ApiResponse(responseCode = "400", description = "Something went wrong", content = @Content),
			@ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
			@ApiResponse(responseCode = "500", description = "Expired or invalid JWT token", content = @Content(schema = @Schema(hidden = true))) }, description = "User Details")
	public ResponseEntity<UserResponse> me(HttpServletRequest req) {
		log.info("Started processing User profile retrieval");
		return ResponseEntity.ok(authService.currentUser(req));
	}

	@GetMapping("/refresh")
	@Operation(summary = "Refresh token", responses = {
			@ApiResponse(responseCode = "200", description = "OK", content = @Content(mediaType = "application/json", schema = @Schema(implementation = AuthResponse.class))),
			@ApiResponse(responseCode = "400", description = "Something went wrong", content = @Content),
			@ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
			@ApiResponse(responseCode = "500", description = "Expired or invalid JWT token", content = @Content(schema = @Schema(hidden = true))) }, description = "Refresh token")
	public ResponseEntity<AuthResponse> refresh(HttpServletRequest req) {
		log.info("Refreshing JWT token");
		return ResponseEntity.ok(authService.refresh(req));
	}

	@GetMapping("/authorizeWithToken/{token}")
	@Operation(summary = "Authorize with token", responses = {
			@ApiResponse(responseCode = "200", description = "OK", content = @Content(mediaType = "application/json", schema = @Schema(implementation = AuthResponse.class))),
			@ApiResponse(responseCode = "403", description = "Something went wrong", content = @Content),
			@ApiResponse(responseCode = "500", description = "Invalid token supplied", content = @Content(schema = @Schema(hidden = true))) }, description = "authorize with token Details")
	public ResponseEntity<AuthResponse> loginWithToken(@PathVariable String token) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.login"), "with token"));
		return ResponseEntity.ok(authService.authorizeWithToken(token));
	}

	@GetMapping("/logout")
	@Operation(summary = "Logout", responses = {
			@ApiResponse(responseCode = "400", description = "OK", content = @Content(schema = @Schema(hidden = true))),
			@ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
			@ApiResponse(responseCode = "500", description = "Expired or invalid JWT token", content = @Content(schema = @Schema(hidden = true))) }, description = "logout")
	public ResponseEntity<?> logout(HttpServletRequest req) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.logout"), ""));
		try {
			req.getSession().setAttribute("FederatedPrincipal", null);
			log.info("Fedarated Details Cleared from Session");
		} catch (Exception ex) {
			log.error(ex.getMessage());
			if (log.isDebugEnabled()) {
				ex.printStackTrace();
			}
		}
		return ResponseEntity.ok(true);
	}

}
