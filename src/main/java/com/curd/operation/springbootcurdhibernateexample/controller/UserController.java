package com.curd.operation.springbootcurdhibernateexample.controller;

import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curd.operation.springbootcurdhibernateexample.domain.User;
import com.curd.operation.springbootcurdhibernateexample.service.UserService;
import com.curd.operation.springbootcurdhibernateexample.util.PropertyLogUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@SecurityRequirement(name = "Authorization")
@RequestMapping({ "/user" })
@Tag(name = "User Controller API", description = "Basic operations for user")
public class UserController {

	@Autowired
	private UserService userService;
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private static Properties properties = PropertyLogUtil.getInstance().getProperties();

	@PostMapping("/save")
	@Operation(summary = "Save", responses = {
			@ApiResponse(responseCode = "200", description = "OK", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Save User Details")
	public ResponseEntity<User> saveUser(@Valid @RequestBody User user) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.save"), "User"));
		return ResponseEntity.ok(userService.saveUser(user));
	}
	
	@PutMapping("/update/{id}")
	@Operation(summary = "Update", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Update User Details")
	public ResponseEntity<User> updateUser(@PathVariable Long id, @Valid @RequestBody User user) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.update"), "User"));
		return ResponseEntity.ok(userService.updateUser(user));
	}
	

	@GetMapping("/get/{id}")
	@Operation(summary = "Get", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Get User Details")
	public ResponseEntity<User> getProductById(@PathVariable Long id) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.get"), "User",id));
		return ResponseEntity.ok(userService.getUserById(id));
	}

	@GetMapping("/get/all")
	@Operation(summary = "Get all", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Get all User Details")
	public ResponseEntity<List<User>> getAllProductDetials() {
		log.info(MessageFormat.format(properties.getProperty("log.desc.retrieved"), "User"));
		return ResponseEntity.ok(userService.getAllUserDetials());
	}

	@DeleteMapping("/delete/{id}")
	@Operation(summary = "Delete", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = HttpStatus.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Delete User Details")
	public void deleteProductById(@PathVariable Long id) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.delete"), "User",id));
		userService.deleteUserById(id);
		ResponseEntity.status(HttpStatus.OK);

	}
	
	@DeleteMapping("/delete/ids")
	@Operation(summary = "Delete all", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = HttpStatus.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Delete Multiple User Details")
	public void deleteUsersWithIds(@Valid @RequestBody List<Long> ids) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.delete"), "Multiple Users",ids));
		userService.deleteUsersWithIds(ids);
		ResponseEntity.status(HttpStatus.OK);

	}
}
