package com.curd.operation.springbootcurdhibernateexample.controller;

import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curd.operation.springbootcurdhibernateexample.domain.Product;
import com.curd.operation.springbootcurdhibernateexample.service.ProductService;
import com.curd.operation.springbootcurdhibernateexample.util.PropertyLogUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@SecurityRequirement(name = "Authorization")
@RequestMapping({ "/product" })
@Tag(name = "Product Controller API", description = "Basic operations for product")
public class ProductController {

	@Autowired
	private ProductService productService;
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private static Properties properties = PropertyLogUtil.getInstance().getProperties();
	
	@PostMapping("/save")
	@Operation(summary = "Save", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Product.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Save Product Details")
	public ResponseEntity<Product> saveProduct(@RequestBody Product product) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.save"), "Product"));
		return ResponseEntity.ok(productService.saveProduct(product));
	}

	@PutMapping("/update/{id}")
	@Operation(summary = "Update", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Product.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Update Product Details")
	public ResponseEntity<Product> updateProduct(@PathVariable Long id, @RequestBody Product product) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.update"), "Product"));
		return ResponseEntity.ok(productService.updateProduct(product));
	}

	@GetMapping("/get/{id}")
	@Operation(summary = "Get", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Product.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Get Product Details")
	public ResponseEntity<Product> getProductById(@PathVariable Long id) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.get"), "Product",id));
		return ResponseEntity.ok(productService.getProductById(id));
	}

	@GetMapping("/get/all")
	@Operation(summary = "Get all", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Product.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Get all Product Details")
	public ResponseEntity<List<Product>> getAllProductDetials() {
		log.info(MessageFormat.format(properties.getProperty("log.desc.retrieved"), "Product"));
		return ResponseEntity.ok(productService.getAllProductDetials());
	}

	@DeleteMapping("/delete/{id}")
	@Operation(summary = "Delete", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = HttpStatus.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Delete Product Details")
	public void deleteProductById(@PathVariable Long id) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.delete"), "Product",id));
		productService.deleteProductById(id);
		ResponseEntity.status(HttpStatus.OK);

	}
	
	@DeleteMapping("/delete/ids")
	@Operation(summary = "Delete all", responses = {
			@ApiResponse(description = "OK", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = HttpStatus.class))),
			@ApiResponse(responseCode = "404", description = "Not found", content = @Content),
			@ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) }, description = "Delete Multiple Product Details")
	public void deleteProductsWithIds(@Valid @RequestBody List<Long> ids) {
		log.info(MessageFormat.format(properties.getProperty("log.desc.delete"), "Multiple Products",ids));
		productService.deleteProductsWithIds(ids);
		ResponseEntity.status(HttpStatus.OK);

	}
	
}
