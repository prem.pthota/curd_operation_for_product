package com.curd.operation.springbootcurdhibernateexample.util;

import java.io.InputStream;
import java.util.Properties;

public class PropertyLogUtil {
	private static PropertyLogUtil propertyLogUtil;

	private java.util.Properties properties;

	private PropertyLogUtil() {
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("log.properties");
		try {
			properties = new Properties();
			properties.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized static PropertyLogUtil getInstance() {
		if (propertyLogUtil == null)
			propertyLogUtil = new PropertyLogUtil();
		return propertyLogUtil;
	}

	public Properties getProperties() {
		return properties;
	}
}
